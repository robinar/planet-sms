<?php

namespace PlanetSMS\Tests;

use PlanetSMS\PlanetSMS;
use PlanetSMS\SMSMessage;

class PlanetSMSTest extends \PHPUnit_Framework_TestCase
{

	/** 
	* @test 
	* @vcr static_send_single.yml
	*/
	public function it_creates_a_static_method_for_sending_single_sms()
	{
		$sms = $this->get_a_stub_sms();

		$result = PlanetSMS::send($sms);

		$this->assertEquals(0, $result[0]->status);
		$this->assertEquals('1234', $result[0]->message_id);

	}



	/** 
	* @test 
	* @vcr static_send_multiple_msg.yml
	*/
	public function it_creates_a_static_method_for_sending_multiple_sms()
	{
		$smses = $this->get_an_array_of_stub_smses();

		$result = PlanetSMS::sendMany($smses);

		$this->assertEquals(0, $result[0]->status);
		$this->assertEquals('1234', $result[0]->message_id);
		$this->assertEquals(0, $result[1]->status);
		$this->assertEquals('1235', $result[1]->message_id);

	}

	/** 
	* @test
	* @vcr static_delivery_report_all.yml 
	*/
	public function it_fetches_all_delivery_report()
	{
		$messages = new SMSMessage(
				'Single message with multiple recipients',  //message
				['8801915054064', '8801915054064'], //recipient
				['multimsg1', 'multimsg2'], 		 //messageid
				'', 			 //sender
				''  			 //datacoding
			);

		$response = PlanetSMS::send($messages);

		$this->assertEquals('multimsg1', $response[0]->message_id);
		$this->assertEquals('multimsg2', $response[1]->message_id);

		$delivery_reports = PlanetSMS::getDeliveryReports();

		$this->assertEquals('multimsg2', $delivery_reports[0]->message_id);
		$this->assertEquals('multimsg1', $delivery_reports[1]->message_id);

	}


	/** @test */
	public function it_parses_delivery_report_xml()
	{
		$file = __DIR__.'/fixtures/delivery_report.xml';

		if(file_exists($file))
		{
			$xml = file_get_contents($file);
		}

		$results = PlanetSMS::parseDeliveryReports($xml);
		
		$this->assertEquals('multimsg2', $results[0]->message_id);
		$this->assertEquals('multimsg1', $results[1]->message_id);
	}


	private function get_a_stub_sms(){
		return new SMSMessage('Test', '8801915054064', '1234');
	}


	private function get_an_array_of_stub_smses(){
		return [
			new SMSMessage('Test', '8801915054064', '1234'),
			new SMSMessage('Test 1', '8801915054064', '1235'),
			];
	}
}