<?php

namespace PlanetSMS\Tests;

use GuzzleHttp\Client as HttpClient;
use Mockery;
use PlanetSMS\PlanetSMSClient;
use PlanetSMS\SMSMessage;



class PlanetSMSClientTest extends \PHPUnit_Framework_TestCase
{
	private $client;

	public function setUp()
	{
		parent::setUp();

		$config = require("src/config/planetsms.php");

		$this->client = new PlanetSMSClient($config['host'], $config['username'], $config['password']);

	}

	public function tearDown()
	{
		parent::tearDown();
	}

	/** @test */
	public function it_creates_a_new_planetgroupsms_client()
	{
		$this->assertInstanceOf(PlanetSMSClient::class, $this->client);
	}

	/** 
	* @test
	* @vcr credit_balance.yml 
	*/
	public function it_gets_the_credit_balance(){
		$this->assertEquals(6059, $this->client->getCreditBalance());
	}


	/**
	* @test
	*/
	public function it_builds_a_json_payload_for_sending(){
		$sms = new SMSMessage('Test Message', ['8801915054064', "8801737546355"], ['111111111111111111', '22222222222222222']);
		$json = <<<"OUT"
JSON=
{
    "authentication": {
        "username": "donateblood",
        "password": "01915054064"
    },
    "messages": {
        "sender": "",
        "text": "Test Message",
        "datacoding": 0,
        "recipients": [
            {
                "gsm": "8801915054064",
                "messageId": "111111111111111111"
            },
            {
                "gsm": "8801737546355",
                "messageId": "22222222222222222"
            }
        ]
    }
}
OUT;

		$this->assertEquals($json, $this->client->buildJsonPayload($sms));
	}



	/** 
	* @test
	* @vcr send_single_sms.yml 
	*/
	public function it_sends_an_sms_with_customized_msg_id(){
		$sms = new SMSMessage('Test Message', '8801915054064', '11111111111111111');

		$response = $this->client->send($sms);

		$this->assertEquals('11111111111111111', $response[0]->message_id);
		$this->assertEquals('8801915054064', $response[0]->destination);
		$this->assertEquals('ALL_RECIPIENTS_PROCESSED', $response[0]->status_message);

	}



	/** 
	* @test
	* @vcr send_single_sms.yml 
	*/
	public function it_sends_an_sms_to_multiple_recipients(){
		$sms = new SMSMessage('Test Message', ['8801915054064', "8801737546355"], ['111111111111111111', '22222222222222222']);

		$response = $this->client->send($sms);

		$this->assertEquals('111111111111111111', $response[0]->message_id);
		$this->assertEquals('22222222222222222', $response[1]->message_id);
	}


	/** 
	* @test
	* @vcr send_single_sms.yml 
	*/
	public function it_sends_multiple_sms_with_multiple_receipient()
	{
		$sms = new SMSMessage('Test Message1', ['8801915054064', "8801737546355"], ['33', '44']);
		$sms1 = new SMSMessage('Test Message2', ['8801915054064', "8801737546355"], ['55', '66']);

		$messages  = [$sms, $sms1];

		$response = $this->client->sendMany($messages);

		$this->assertEquals('33', $response[0]->message_id);
		$this->assertEquals('44', $response[1]->message_id);	
		$this->assertEquals('55', $response[2]->message_id);
		$this->assertEquals('66', $response[3]->message_id);
	}



	/** 
	* @test
	* @vcr send_single_sms.yml 
	*/
	public function it_sends_a_scheduled_sms()
	{
		$sms = new SMSMessage('Test Message1', ['8801915054064'], ['33']);

		$sms->setSchedule('0d0h1m');

		$response = $this->client->send($sms);

		$this->assertEquals('33', $response[0]->message_id);
	}



	/** 
	* @test
	* @vcr send_single_sms.yml 
	*/
	public function it_sends_a_unicode_sms()
	{
		$sms = new SMSMessage('আমার নাম রবিন', ['8801915054064'], ['33']);

		$response = $this->client->send($sms);

		$this->assertEquals('0', $response[0]->status);
		$this->assertEquals('33', $response[0]->message_id);
	}


	/** 
	* @test
	* @vcr send_single_sms.yml 
	*/
	public function it_sends_a_unicode_flash_sms()
	{
		$sms = new SMSMessage('বাংলা ফ্লাশ মেসেজ', ['8801915054064'], ['33']);

		$sms->setFlash();

		$response = $this->client->send($sms);

		$this->assertEquals('0', $response[0]->status);
		$this->assertEquals('33', $response[0]->message_id);
	}




	/** 
	* @test
	* @vcr send_single_sms.yml 
	*/
	public function it_sends_a_english_flash_sms()
	{
		$sms = new SMSMessage('Flash message', ['8801915054064'], ['33']);

		$sms->setFlash();

		$response = $this->client->send($sms);

		$this->assertEquals('0', $response[0]->status);
		$this->assertEquals('33', $response[0]->message_id);
	}




	/** 
	* @test
	* @vcr delivery_report_all.yml 
	*/
	public function it_fetches_all_delivery_report()
	{
		$messages = $this->create_stub_text_message_with_multiple_recipient();

		$response = $this->client->send($messages);

		$this->assertEquals('multimsg1', $response[0]->message_id);
		$this->assertEquals('multimsg2', $response[1]->message_id);

		$delivery_reports = $this->client->getDeliveryReports();

		$this->assertEquals('multimsg2', $delivery_reports[0]->message_id);
		$this->assertEquals('multimsg1', $delivery_reports[1]->message_id);

	}



	/** 
	* @test
	* @vcr delivery_report_single.yml 
	*/
	public function it_fetches_a_delivery_report()
	{
		$messages = $this->create_stub_text_message();

		$response = $this->client->send($messages);

		$this->assertEquals('singlemsgid1', $response[0]->message_id);

		$delivery_reports = $this->client->getDeliveryReports('singlemsgid1');

		$this->assertEquals('singlemsgid1', $delivery_reports[0]->message_id);
	}	



	public function create_stub_text_message()
	{
		return new SMSMessage(
				'Text message',  //message
				'8801915054064', //recipient
				'singlemsgid1', 		 //messageid
				'', 			 //sender
				'' 			 //datacoding
			);

	}


	public function create_stub_text_message_with_multiple_recipient()
	{
		return new SMSMessage(
				'Single message with multiple recipients',  //message
				['8801915054064', '8801915054064'], //recipient
				['multimsg1', 'multimsg2'], 		 //messageid
				'', 			 //sender
				''  			 //datacoding
			);
	}


	public function create_stub_unicode_message()
	{
		return new SMSMessage(
				'বাংলা মেসেজ',  //message
				'8801915054064', //recipient
				'unicodemsgid1', 		 //messageid
				'', 			 //sender
				''  			 //datacoding
			);
	}



}
?>
