<?php

namespace PlanetSMS\Tests;

use PlanetSMS\SMSMessage;


class SMSMessageTest extends \PHPUnit_Framework_TestCase
{

	public function setUp()
	{
		parent::setUp();
	}

	public function tearDown()
	{
		parent::tearDown();
	}


	/** @test*/
	public function it_creates_a_new_instance_of_smsmessage()
	{
		$sms = new SMSMessage('Test message', '017');
		$this->assertInstanceOf(SMSMessage::class, $sms);
	}

	/** @test */
	public function it_creates_a_new_instance_of_smsmessage_from_static_method()
	{
		$sms = SMSMessage::create('Test message', '017');
		$this->assertInstanceOf(SMSMessage::class, $sms);
	}


	/** @test*/
	public function it_sets_and_gets_the_same_msg_from_smsmessage()
	{
		$msg = 'Test message';
		$msg1 = 'Test message1';

		$sms = new SMSMessage($msg, '017');
		$this->assertTrue($msg == $sms->getText());

		$sms->setText($msg1);
		$this->assertTrue($msg1 == $sms->getText());
	}

	/** @test*/
	public function it_sets_either_string_or_array_to_recipients_and_returns_arrayof_recipients()
	{
		$msg = 'Test message';
		$recipients1 = "017";
		$recipients2 = ["017", "016"];

		$sms = new SMSMessage($msg, $recipients1);
		$this->assertEquals([$recipients1], $sms->getRecipients());


		$sms = new SMSMessage($msg, $recipients2);
		$this->assertEquals($recipients2, $sms->getRecipients());

	}


		/** @test*/
	public function it_sets_and_gets_sender()
	{
		$msg = 'Test message';
		$recipients1 = "017";
		$messageid = '11111';
		$sender1 = "ABC";
		$sender2 = "XYZ";

		$sms = new SMSMessage($msg, $recipients1, $messageid, $sender1);
		$this->assertEquals($sender1, $sms->getSender());

	
		$sms->setSender($sender2);
		$this->assertEquals($sender2, $sms->getSender());

	}


	/** @test */
	public function it_sets_custom_datacoding()
	{
		$msg = 'Test message';
		$recipients1 = "017";	
		$messageid = '11111';
		$sender1 = "ABC";
		$datacoding1 = 12;

		$sms = new SMSMessage($msg, $recipients1, $messageid, $sender1, $datacoding1);
		$this->assertEquals($datacoding1, $sms->getDatacoding());
	}

	/** @test */
	public function it_detectcs_correct_datacoding()
	{
		$ascii_msg = 'Test message';
		$unicode_msg = "আমার নাম রবিন";
		$recipients1 = "017";	

		$sms = new SMSMessage($ascii_msg, $recipients1);
		$this->assertEquals(0, $sms->getDatacoding());

		$sms->setText($unicode_msg);
		$this->assertTrue($sms->isUnicode());
		$this->assertEquals(8, $sms->getDatacoding());
	}

	/** @test */
	public function it_sets_and_unset_correct_datacoding_for_flash_sms()
	{
		$ascii_msg = 'Test message';
		$unicode_msg = "আমার নাম রবিন";
		$recipients1 = "017";
		$messageid = '11111';
		$sender1 = "ABC";
		$flash = true;

		$sms = new SMSMessage($ascii_msg, $recipients1, $messageid, $sender1, '', $flash);
		$this->assertEquals('240', $sms->getDatacoding());
		
		$sms->unsetFlash();
		$this->assertEquals('0', $sms->getDatacoding());

		$sms = new SMSMessage($unicode_msg, $recipients1, $messageid, $sender1, '', $flash);
		$this->assertEquals('24', $sms->getDatacoding());
		
		$sms->unsetFlash();
		$this->assertEquals('8', $sms->getDatacoding());

	}

	/** @test */
	public function it_gets_message_length()
	{
		$ascii_msg = 'Test message';
		$unicode_msg = "আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন ";
		$recipients1 = "017";	

		$sms = new SMSMessage($ascii_msg, $recipients1);
		$this->assertEquals(12, $sms->getMessageLength());

		$sms->setText($unicode_msg);
		$this->assertEquals(168, $sms->getMessageLength());
	}

	/** @test */
	public function it_gets_message_count()
	{
		$ascii_msg = 'Test message';
		$unicode_msg = "আমার নাম রবিন";
		$recipients1 = "017";	

		$sms = new SMSMessage($ascii_msg, $recipients1);
		$this->assertEquals(1, $sms->getSMSCount());

		$sms->setText($unicode_msg);
		$this->assertEquals(1, $sms->getSMSCount());


		$ascii_msg = 'Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message Test message ';
		$unicode_msg = "আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন আমার নাম রবিন ";

		$sms = new SMSMessage($ascii_msg, $recipients1);
		$this->assertEquals(2, $sms->getSMSCount());

		$sms->setText($unicode_msg);
		$this->assertEquals(3, $sms->getSMSCount());
	}


	/** @test */
	public function it_gets_array_output_from_the_object()
	{
		$text = "Test message";
		$recipients = ['01','02'];
		$messageid = ['11111', '22222'];
		$sender = "ABC";

		$output = [
			'sender' => $sender,
			'text' => $text,
			'datacoding' => 240,
			'recipients' => [
				['gsm' => '01', 'messageId' => '11111'],
				['gsm' => '02', 'messageId' => '22222']
			],
			'sendDateTime' => '0d0h10m',
		];
		$sms = new SMSMessage($text, $recipients, $messageid, $sender, 0, true);
		$sms->setSchedule('0d0h10m');

		$this->assertEquals($output, $sms->toArray());

	}

	/** 
	* @test 
	* @expectedException Exception
	* @expectedExceptionMessage Recipients and messageid array length must be of same
	*/
	public function it_throws_exception_if_receipients_and_messageid_length_doesnt_match()
	{
		$text = "Test message";
		$recipients = ['01','02'];
		$messageid = ['11111'];
		$sender = "ABC";

		$this->expectException(\Exception::class);

		$sms = new SMSMessage($text, $recipients, $messageid, $sender);

	}

	/** @test */
	public function it_sets_and_gets_schedule_time()
	{
		$text = "Test message";
		$recipients = ['01','02'];
		$messageid = '';
		$sender = "ABC";

		$sms = new SMSMessage($text, $recipients, $messageid, $sender);

		$sms->setSchedule('0d0h10m');

		$this->assertEquals('0d0h10m', $sms->getSchedule());
		
		$sms->unsetSchedule();
		$this->assertNull($sms->getSchedule());

	}


 	/** 
	* @test 
	* @expectedException Exception
	* @expectedExceptionMessage Invalid schedule time format
	*/
	public function it_throws_exception_if_invalid_schedule_time_is_given()
	{
		$text = "Test message";
		$recipients = ['01','02'];
		$messageid = '';
		$sender = "ABC";

		$this->expectException(\Exception::class);

		$sms = new SMSMessage($text, $recipients, $messageid, $sender);

		$sms->setSchedule('0d0hm');
	}


}

?>