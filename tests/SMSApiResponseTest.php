<?php
namespace PlanetSMS\Tests;

use PlanetSMS\SMSApiResponse;

class SMSApiResponseTest extends \PHPUnit_Framework_Testcase{

	private $result = array (
					  'status' => '0',
					  'messageid' => '197010319312278343',
					  'destination' => '8801915054064',
					);

	private $api_response;

	public function setUp()
	{
		parent::setUp();

		$this->api_response = new SMSApiResponse($this->result);
	}



	/**
	* @test
	*/
	public function it_gets_an_instance_of_class()
	{
		$this->assertInstanceOf(SMSApiResponse::class, $this->api_response);
	}



	/**
	* @test
	*/
	public function it_returns_status_message()
	{
		$this->assertEquals('ALL_RECIPIENTS_PROCESSED', $this->api_response->status_message);
	}



	/**
	* @test
	*/
	public function it_returns_status_description()
	{
		$this->assertEquals('Request was successful. (all recipients) ', $this->api_response->status_description);
	}

}
?>