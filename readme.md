Installation
------------

Include the follwing service provider alias within config/app.php.

'providers' => [
    PlanetSMS\PlanetSMSProvider::class,
];

'aliases' => [
	'PlanetSMS' => PlanetSMS\PlanetSMSChannel::class,
];

Issue the follwing command in the poject root to copy the config file.

php artisan vendor:publish --provider='PlanetSMS\PlanetSMSProvider'

Edit the planetsms.php config file with the appropriate values

'base_uri' => 'http://example.com/api/v3/',
'credit_cmd_url' => 'http://example.com/api/command?username=%s&password=%s&cmd=Credits',
'username' => 'user',
'password' => 'pass' 

To parse the DeliveryReports pushed from sms gateway, add the following code in the controller/route action - 

$content =  request()->getContent();
$dr = PlanetSMS\PlanetSMS::parseDeliveryReports($content);

Done!
