<?php

namespace PlanetSMS\Facades;

use Illuminate\Support\Facades\Facade;
use PlanetSMS\PlanetSMSClient;


class PlanetSMS extends Facade
{

	Protected static function getFacadeAccessor()
	{
		return PlanetSMSClient::class;
	}
}

?>