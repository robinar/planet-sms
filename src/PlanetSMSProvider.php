<?php

namespace PlanetSMS;

use Illuminate\Support\ServiceProvider;


class PlanetSMSProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
                __DIR__.'/config/planetsms.php' => config_path('planetsms.php')
            ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(PlanetSMSClient::class, function($app){
            $config = config('planetsms');

            if(is_null($config)){
                throw new \Exception('Please configure PlanetSMS before using it. (use> php artisan vendor:publish)');
            }

            return new PlanetSMSClient($config['base_uri'], $config['username'], $config['password'], $config['credit_cmd_url']);
        });

        $this->app->bind(PlanetSMSChannel::class, function($app){
            return new PlanetSMSChannel($app->make(PlanetSMSClient::class));
        });

    }
}
