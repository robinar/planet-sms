<?php

namespace PlanetSMS;

use GuzzleHttp\Client as HttpClient;
use PlanetSMS\SMSMessage;
use PlanetSMS\SMSApiResponse;
use PlanetSMS\SMSDeliveryReport;

class PlanetSMSClient
{
	const API_BASE_PATH = '/api/v3/';

	const CREDIT_CMD_PATH = '/api/command?username=%s&password=%s&cmd=Credits';

	const DELIVERY_REPORT_PATH = '/api/v3/dr/pull?user=%s&password=%s';

	private $httpClient;

	private $username;

	private $password;

	private $base_url;

	private $credit_cmd_url;
	
	private $delivery_report_url;


	public function __construct($host, $username, $password)
	{
		$this->base_url = $host.self::API_BASE_PATH;
		$this->credit_cmd_url = $host.self::CREDIT_CMD_PATH;
		$this->delivery_report_url = $host.self::DELIVERY_REPORT_PATH;

		$this->httpClient = new HttpClient([
				'base_uri' => $this->base_url
			]);
		$this->username = $username;
		$this->password = $password;
	}


	public function send(SMSMessage $message)
	{
		$body = $this->buildJsonPayload($message);
		return $this->sendToApi($body);
	}


	public function sendMany(Array $messages)
	{
		$result = [];
		foreach($messages as $message){
			$response = $this->send($message);
			$result = array_merge($result, $response);
		}

		return $result;
	}


	private function sendToApi($body)
	{
		$response = $this->httpClient->post('sendsms/json', [
			'body' => $body
		]);

		$content = $response->getBody()->getContents();
		
		return $this->parseContent($content);	
	}



	public function buildJsonPayload(SMSMessage $sms){
		$auth = $this->getAuthHeader();

		$messages = [ 'messages' => $sms->toArray()];

		$payload = "JSON=\n".json_encode(array_merge($auth, $messages), JSON_PRETTY_PRINT);

		return $payload;
	}


	private function getAuthHeader(){
		return [
		'authentication' => [
			'username' => $this->username,
			'password' => $this->password
		]];
	}



	public function getCreditBalance()
	{
		$query_url = sprintf($this->credit_cmd_url, $this->username, $this->password);

		$response = $this->httpClient->get($query_url);

		return (int) $response->getBody()->getContents();
	}


	private function parseContent($content)
	{
		$content = json_decode($content, true);

		$output = [];  

		foreach($content['results'] as $result)
		{
			$output[] = new SMSApiResponse($result);
		}

		return $output;
	}

	public function getDeliveryReports($message_id = '')
	{
		$empty_result = 'NO_DATA';
		$errors = ['MISSING_USERNAME', 'MISSING_PASSWORD', 'AUTH_FAILED'];
		$delivery_report_url  = $this->makeDeliveryReportUrl($message_id);

		$response =  $this->httpClient->get($delivery_report_url);
		$content = $response->getBody()->getContents();

		if(in_array($content, $errors))
		{
			throw new \Exception('DeliveryReport API authentication failure. Please check if username and passwords are correct.');
		}

		if($content == $empty_result)
		{
			return '';
		}

		return $this->parseDeliveryReports($content);
	}


	private function makeDeliveryReportUrl($message_id = '')
	{
		if($message_id != ''){
			return sprintf($this->delivery_report_url."&messageid=%s", $this->username, $this->password, $message_id);
		}

		return sprintf($this->delivery_report_url, $this->username, $this->password);		
	}



	public function parseDeliveryReports($content){
		$dom = new \DOMDocument();
		$dom->loadXML($content);
		$xpath = new \DOMXPath($dom);
		$query = '/DeliveryReport/message';

		$reports  = $xpath->query($query);

		$delivery_reports = [];

		foreach ($reports as $report) {
			$delivery_reports[] = new SMSDeliveryReport(
					$report->getAttribute('id'), 
					$report->getAttribute('sentdate'), 
					$report->getAttribute('donedate'), 
					$report->getAttribute('status'));
		}

		return $delivery_reports;
	}

}
?>