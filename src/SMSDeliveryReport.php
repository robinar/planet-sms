<?php namespace PlanetSMS;

class SMSDeliveryReport
{
    public $message_id;
    public $sent_date;
    public $done_date;
    public $status;
    public $status_description;

    const STATUSES = [
			     "NOT SENT" => "The message is queued in the Infobip system but cannot be submitted to SMSC (possible reason: SMSC connection is down)",
			     "SENT" => "The message was sent over a route that does not support delivery reports",
			     "NOT DELIVERED" => "The message could not be delivered",
			     "DELIVERED" => "The message was successfully delivered to the recipient",
			     "NOT ALLOWED" => "The client has no authorization to send to the specified network (the message will not be charged)",
			     "INVALID DESTINATION ADDRESS" => "Invalid/incorrect GSM recipient",
			     "INVALID SOURCE ADDRESS" => "You have specified incorrect/invalid/not allowed source address (sender name)",
			     "ROUTE NOT AVAILABLE" => "You are trying to use routing that is not available for your account",
			     "NOT ENOUGH CREDITS" => "There are no available credits on your account to send the message",
			     "REJECTED" => "Message has been rejected, reasons may vary",
			   ];

    public function __construct($messageId, $sentDate, $doneDate, $status)
    {
    	$this->message_id = $messageId;
    	$this->sent_date = $sentDate;
    	$this->done_date = $doneDate;
    	$this->status = $status;
    	$this->status_description = self::STATUSES[$status];
    }

}