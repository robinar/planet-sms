<?php

namespace PlanetSMS;

class SMSApiResponse
{
	const STATUSES = [
	    0 => [
	        'status' => 'ALL_RECIPIENTS_PROCESSED',
	        'description' => 'Request was successful. (all recipients) ',
	    ],
		-1 => [
	        'status' => 'SEND_ERROR',
	        'description' => 'Error in processing the request',
	    ],
		-2 => [
	        'status' => 'NOT_ENOUGH_CREDITS',
	        'description' => 'Not enough credits on a specific account',
	    ],
		-3 => [
	        'status' => 'NETWORK_NOTCOVERED',
	        'description' => 'Targeted network is not covered on specific account ',
	    ],
		-5 => [
	        'status' => 'INVALID_USER_OR_PASS',
	        'description' => 'Username or password is invalid',
	    ],
		-6 => [
	        'status' => 'MISSING_DESTINATION_ADDRESS',
	        'description' => 'Destination address is missing in the request ',
	    ],
		-10 => [
	        'status' => 'MISSING_USERNAME',
	        'description' => 'Username is missing in the request',
	    ],
		-11 => [
	        'status' => 'MISSING_PASSWORD',
	        'description' => 'Password is missing in the request',
	    ],
		-13 => [
	        'status' => 'INVALID_DESTINATION_ADDRESS',
	        'description' => 'Number is not recognized by Infobip platform ',
	    ],
		-22 => [
	        'status' => 'XML_ERROR',
	        'description' => 'Incorrect XML format, caused by syntax error ',
	    ],
		-23 => [
	        'status' => 'ERROR_PROCESSING',
	        'description' => 'General error, reasons may vary',
	    ],
		-26 => [
	        'status' => 'COMMUNICATION_ERROR',
	        'description' => 'General. API error, reasons may vary',
	    ],
		-27 => [
	        'status' => 'INVALID_SENDDATETIME',
	        'description' => 'Invalid scheduling parametar',
	    ],
		-28 => [
	        'status' => 'INVALID_DELIVERY_REPORT_PUSH_URL',
	        'description' => 'Invalid PushURL in the request',
	    ],
		-29 => [
	        'status' => 'JSON_ERROR',
	        'description' => 'Incorrect Json format, caused by syntax error ',
	    ],
		-30 => [
	        'status' => 'INVALID_CLIENT_APPID',
	        'description' => 'Invalid APPID in the request',
	    ],
		-33 => [
	        'status' => 'DUPLICATE_MESSAGEID',
	        'description' => 'Duplicated MessagelD in the request',
	    ],
		-34 => [
	        'status' => 'SENDER_NOT_ALLOWED',
	        'description' => 'Sender name is not allowed',
	    ],
		-99 => [
	        'status' => 'GENERAL_ERROR',
	        'description' => 'Error in processing request, reasons may vary',
	    ],
	];

	public $status;

	public $message_id;

	public $destination;

	public $status_message;

	public $status_description;



	function __construct( $result)
	{
		$this->status = $result['status'];
		$this->message_id = $result['messageid'];
		$this->destination = $result['destination'];

		$this->status_message = $this->getStatusMessage($this->status);
		$this->status_description = $this->getStatusDescription($this->status);
	}



	public function getStatusMessage($code){
		return self::STATUSES[$code]['status'];
	}


	public function getStatusDescription($code){
		return self::STATUSES[$code]['description'];
	}

}
?>