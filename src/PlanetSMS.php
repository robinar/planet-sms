<?php
namespace PlanetSMS;

use PlanetSMS\PlanetSMSClient;
use PlanetSMS\SMSMessage;

class PlanetSMS
{
	private $client;


	private function __construct()
	{
		$config = require('config/planetsms.php');

		$this->client = new PlanetSMSClient($config['host'], $config['username'], $config['password']);
	}


	static function send(SMSMessage $message)
	{
		$instance = new self();
		return $instance->client->send($message);
	}


	static function sendMany(Array $messages)
	{
		$instance = new self();
		return $instance->client->sendMany($messages);
	}


	static function getCreditBalance()
	{
		$instance = new self();
		return $instance->client->getCreditBalance();
	}


	static function getDeliveryReports($message_id = '')
	{
		$instance = new self();
		return $instance->client->getDeliveryReports($message_id);
	}


	static function parseDeliveryReports($content)
	{
		$instance = new self();
		return $instance->client->parseDeliveryReports($content);
	}
}

?>