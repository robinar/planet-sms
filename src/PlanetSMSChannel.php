<?php
namespace PlanetSMS;

use Illuminate\Notifications\Notification;
use PlanetSMS\PlanetSMSClient;

class PlanetSMSChannel
{

	public function __construct(PlanetSMSClient $client)
	{
		$this->client = $client;
	}



	public function send($notifiable, Notification $notification)
	{
		$message  = $notification->toPlanetSMS($notifiable);

		//Get the recipient from notifiable entity i.e. User
		$to = $notifiable->routeNotificationFor('PlanetSMS');

		$this->client->send($message);
	}



}
?>