<?php

namespace PlanetSMS;

class SMSMessage{

	private $text;

	private $recipients= [];

	private $messageid;

	private $sender;

	/**
	* ASCII => 0, UTF-8 => 8, ASCII Flash => 240, Unicode Flash => 24
	*/
	private $datacoding;

	private $schedule_time;

	private $sms_count;

	private $unicode;


	const ASCII_SMS_LENGTH = ['single' => 160, 'multi' => 153];

	const UNICODE_SMS_LENGTH = ['single' => 70, 'multi' => 67];



	public function __construct($text, $recipients, $messageid = '', $sender = '', $datacoding = '', $flash = '')
	{
		$this->text  = $text;
		$this->detectUnicode();
		$this->setRecipients($recipients);
		
		if(!empty($messageid)){
			$this->setMessageid($messageid);
		}

		$this->sender = $sender;
		$this->setDatacoding($datacoding);
		
		if($flash == true){
			$this->setFlash();			
		}

		$this->setSMSCount();
	}



	public static function create($text, $recipients, $sender = '', $datacoding = '', $flash = ''){
		return new static($text, $recipients, $sender, $datacoding, $flash);
	}



	public function setText($text)
	{
		$this->text = $text;
		$this->detectUnicode();
		$this->setDatacoding();
		$this->setSMSCount();
	}



	public function getText()
	{
		return $this->text;
	}



	public function setRecipients($recipients)
	{
		if(!is_array($recipients)){
			$this->recipients = [$recipients];
			return;
		}

		$this->recipients = $recipients;
	}



	public function getRecipients()
	{
		return $this->recipients;
	}


	
	public function setSender($sender)
	{
		$this->sender = $sender;
	}



	public function getSender()
	{
		return $this->sender;
	}


	public function setMessageid($messageid)
	{
		if(is_string($messageid))
		{
			$this->messageid = [$messageid];
		}else{
			$this->messageid = $messageid;		
		}

		if(count($this->messageid) != count($this->recipients)){
			throw new \Exception('Recipients and messageid array length must be of same');
		}
	}



	public function getMessageid()
	{
		return $this->messageid;
	}



	public function setDatacoding($datacoding = '')
	{

		if($datacoding == '')
		{
			$this->autoDatacoading();
			return;
		} 

		$this->datacoding = $datacoding;
	}



	private function autoDatacoading()
	{
		if($this->isUnicode())
		{
			$this->datacoding = 8;
			return;
		}
		
		$this->datacoding = 0;
	}



	public function getDatacoding()
	{
		return $this->datacoding;
	}


	public function setFlash()
	{
		if($this->isUnicode())
		{
			$this->datacoding = 24;
			return;
		}

		$this->datacoding = 240;
	}



	public function unsetFlash()
	{
		$this->autoDatacoading();
	}



	public function isFlash()
	{
		if($this->datacoding == 240 || $this->datacoding == 24){
			return true;
		}
	}



	public function setSchedule($time)
	{
		if(!preg_match("/\d+d\dh\d+m/", $time))
		{
			throw new \Exception("Invalid schedule time format");			
		}

		$this->schedule_time = $time;
	}



	public function unsetSchedule()
	{
		$this->schedule_time = null;
	}



	public function getSchedule()
	{
		return $this->schedule_time;
	}	


	public function setSMSCount()
	{
		if($this->isUnicode()){
			$this->setUnicodeMessageCount();
			return;
		}

		$this->setASCIIMessageCount();

	}



	public function getSMSCount()
	{
		return $this->sms_count;
	}



	public function getMessageLength()
	{
		if($this->isUnicode())
		{
			return mb_strlen($this->text);
		}

		return strlen($this->text);
	}



	private function setUnicodeMessageCount()
	{
		$length = $this->getMessageLength();
		$count = 1;

		if($length > self::UNICODE_SMS_LENGTH['single']){
			$count = ceil($length/self::UNICODE_SMS_LENGTH['multi']);
		}

		$this->sms_count = $count;
	}



	private function setASCIIMessageCount()
	{
		$length = $this->getMessageLength();
		$count = 1;

		if($length > self::ASCII_SMS_LENGTH['single']){
			$count = ceil($length/self::ASCII_SMS_LENGTH['multi']);
		}

		$this->sms_count = $count;
	}



	private function detectUnicode(){
		$this->unicode = mb_detect_encoding($this->text, 'auto', true) == 'UTF-8'; 
	}


	public function isUnicode()
	{
		return $this->unicode;
	}



	public function __toString()
	{
		return $this->text;
	}



	public function toArray()
	{
		$array = [];
		
		$array['sender'] = $this->sender;
		$array['text']   = $this->text;
		$array['datacoding'] = $this->datacoding;
		$array['recipients'] = $this->getFormattedRecipients();

		if(!empty($this->schedule_time)){
			$array['sendDateTime']   = $this->schedule_time;
		}

		return $array;
	}



	public function toJSON()
	{
		return json_encode($this->toArray());
	}



	private function getFormattedRecipients(){
		$output= [];

		for ($i=0; $i < count($this->recipients) ; $i++) { 
			$output[$i] = [
				'gsm' => $this->recipients[$i],
				'messageId' => $this->messageid[$i]
			];					
		}		

		return $output;
	}

}

?>